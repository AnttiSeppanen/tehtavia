console.log ("toimii") //toimiiko koodi
// määritellään muuttuja joka on vakio
// hae canvaasi id:llä oleva elementti
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
// vanha var
let raf;
let inputStates = {} // tänne talteen näppäin painallukset
window.addEventListener('keydown', function(event) {
console.log (event.key);
if (event.key == "ArrowRight") {
  inputStates.right = true;
}

if (event.key == "ArrowLeft") {
  inputStates.left = true;
}
}, false) ;
window.addEventListener('keyup', function(event) {
  console.log (event.key);
  if (event.key == "ArrowRight") {
    inputStates.right = false;
  }
  if (event.key == "ArrowLeft") {
    inputStates.left = false;
  }

}, false) ;





//pallon rakenne
const ball = {
  x: 100,
  y: 100,
  vx: 9, // nopeus ja suunta
  vy: 2,
  radius: 10,
  color: 'blue',
  draw() { // pallon piirto
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fillStyle = this.color;
    ctx.fill();
  }
  };
  const maila = {
  
        x: 300,
        y: 285,
        vx: 11,
        leveys: 100,
        korkeus: 15,
        color: 'black',
        draw() {
          ctx.fillStyle = this.color;
          ctx.fillRect(this.x, this.y, this.leveys, this.korkeus);
        }
      };
  

//pelin toiminta
function draw() {
  ctx.clearRect(0,0, canvas.width, canvas.height);
  ball.draw();
  maila.draw();
  // liikuttaa objekteja
  ball.x += ball.vx;
  ball.y += ball.vy;
  //liikuta mailaa
  if (inputStates.right) {
    maila.x = maila.x + maila.vx;
  }
  if (inputStates.left){
    maila.x = maila.x - maila.vx;
  } 
// boundaries
  if (ball.y + ball.radius > canvas.height ||
      ball.y - ball.radius < 0) {
    ball.vy = -ball.vy;
  }
  if (ball.x + ball.radius > canvas.width ||
      ball.x - ball.radius < 0) {
    ball.vx = -ball.vx;
  }
  //mailaseinään
  if (maila.x < 0){
maila.x = 0;
  }
if (maila.x + maila.leveys > canvas.width){
maila.x = canvas.width - maila.leveys;

}
// pallo mailaan

raf = window.requestAnimationFrame(draw);

var testX=ball.x;
  var testY=ball.y;

  if (testX < maila.x) testX=maila.x;
  else if (testX > (maila.x+maila.leveys)) testX=(maila.x+maila.leveys);
  if (testY < maila.y) testY=maila.y;
  else if (testY > (maila.y+maila.korkeus)) testY=(maila.y+maila.korkeus);

  var distX = ball.x - testX;
  var distY = ball.y - testY;
  var dista = Math.sqrt((distX*distX)+(distY*distY));
  if (dista <= ball.radius) {
    if (ball.x >= maila.x &&
      ball.x <= (maila.x+maila.leveys)) {
        ball.vy *= -1;
      }
      else if (ball.y >= maila.y &&
        ball.y <= (maila.y+maila.korkeus)) {
          ball.vx *= -1;
        }
  }
};
draw(); //suorillaa funktion
