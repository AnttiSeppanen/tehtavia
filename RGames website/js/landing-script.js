const lines = document.querySelectorAll(".line");
const logo = document.querySelector("#logo-container");

var contentRevealed = false;
function RevealContent(){
    lines.forEach((line, i) => {
        setTimeout(()=>{
            line.classList.toggle("hidden", false);
        }, 250 * i);
    });
    contentRevealed = true;
}

var logoRevealed = false;
var logoExpanded = false;
var expandLogoAfterReveal = false;
function RevealLogo(){
    setTimeout(()=>{ logo.classList.toggle("hidden", false); }, 250);
    setTimeout(()=>{ 
        logoRevealed = true; 
        if(expandLogoAfterReveal){
            ExpandLogo();
        }
    }, 2000);
}

function ExpandLogo(){
    logo.classList.toggle("hidden", false);
    logo.classList.toggle("expanded", true);
    logoExpanded = true;
}

RevealLogo();

window.addEventListener("scroll", (e) => { 
    
    if(!contentRevealed){
        top_scrollPrc = window.scrollY / window.innerHeight;
        if(top_scrollPrc > 0.1) RevealContent();
    }
    
    if(!logoExpanded){
        top_scrollPrc = window.scrollY / window.innerHeight;
        if(top_scrollPrc > 0.05){
            if(logoRevealed){
                ExpandLogo();
            }else{
                expandLogoAfterReveal = true;
            }
        }
    }

}, false);

function GetMaxScroll(){
    return Math.max( 0, Math.max( document.body.scrollHeight, 
                        document.body.offsetHeight, 
                        document.documentElement.clientHeight, 
                        document.documentElement.scrollHeight, 
                        document.documentElement.offsetHeight ) - window.innerHeight);
}

window.onbeforeunload = function () {
    window.scrollTo(0, 0);
}