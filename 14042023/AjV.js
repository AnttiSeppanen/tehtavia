window.onload = paramerit;
function paramerit() {
    const queryString = window.location.search;
    console.log(queryString);
    const urlParams = new URLSearchParams(queryString);

    const Age = urlParams.get('ika');
    console.log(Age);
    document.getElementById("ika").innerHTML = "Ikä: " + Age;

    const sposti = urlParams.get('sposti');
    console.log(sposti);
    document.getElementById("sposti").innerHTML = "Email: " + sposti;

    const aihe = urlParams.get('aihe');
    console.log(aihe);
    document.getElementById("aihe").innerHTML = "Aihe: " + aihe;

    const Paikka = urlParams.get('paikka');
    console.log(Paikka);
    document.getElementById("paikka").innerHTML = "Paikka: " + Paikka;

    const Date = urlParams.get('date');
    console.log(Date);
    document.getElementById("date").innerHTML = "Päiväys: " + Date;

    const Klo = urlParams.get('klo');
    console.log(Klo);
    document.getElementById("klo").innerHTML = "Kello: " + Klo;
}