function check() {
    let x = document.forms["formi"]["ika"].value;
    if (x == "") {
      alert("Ikä pitää täyttää");
      return false;
    }
    let k = document.getElementById("ika").value;
    // If x is Not a Number or less than one or greater than 10
    if (isNaN(k) || k < 1 || k > 120) {
      alert("Iän pitää olla yli 0 ja alle 120");
      return false;
    }

    let y = document.forms["formi"]["sposti"].value;
    if (y == "") {
      alert("sposti pitää täyttää");
      return false;
    }

    let z = document.forms["formi"]["aihe"].value;
    if (z == "") {
      alert("Aihe pitää täyttää");
      return false;
    }

    let r = document.forms["formi"]["paikka"].value;
    if (r == "") {
      alert("Paikka pitää täyttää");
      return false;
    }

    let g = document.forms["formi"]["date"].value;
    if (g == "") {
      alert("Päiväys pitää täyttää");
      return false;
    }

    let j = document.forms["formi"]["klo"].value;
    if (j == "") {
      alert("Kellonaika pitää täyttää");
      return false;
    }
  } 